package com.test.controller;

import com.test.dto.Customer;
import com.test.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @ApiOperation(value = "/customers", nickname = "customer", notes = "Create a new customer", response = Customer.class, tags={  })
    @ApiResponses(value = {
      @ApiResponse(code = 200, message = "OK", response = Customer.class)})
    @PostMapping({"/customers/"})
    @ResponseBody
    public ResponseEntity createCustomer(@RequestBody @Valid Customer customer) {

        Customer response = customerService.createCustomer(customer);
        return ResponseEntity.ok().body(response);
    }

    @ApiOperation(value = "/customers", nickname = "customers", notes = "Gets a customer", response = Customer.class, tags={  })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = Customer.class)})
    @GetMapping({ "/customers/{customerId}"})
    @ResponseBody
    public ResponseEntity getCustomer(@PathVariable(required = false, name = "customerId") Integer customerId) {

        Customer response = customerService.getCustomerById(customerId);
        return ResponseEntity.ok().body(response);
    }

}
