package com.test.service;

import com.test.dto.Customer;
import com.test.exception.CustomerNotFoundException;
import com.test.exception.CustomerNotValidException;
import com.test.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CustomerRepository customerRepository;


    public Customer createCustomer(Customer customer) {

        if(customer.getCreatedAt().isAfter(LocalDate.now())) {
            throw new CustomerNotValidException("Created at needs to be in the past");
        }
        customer.setExternalId(UUID.randomUUID());

        return customerRepository.save(customer);
    }

    public Customer getCustomerById(int customerId) {

        Optional<Customer> byId = customerRepository.findById(customerId);
        if(byId.isPresent()) {
            return byId.get();
        }
        throw new CustomerNotFoundException("Customer not found for id "+ String.valueOf(customerId));
    }

}
