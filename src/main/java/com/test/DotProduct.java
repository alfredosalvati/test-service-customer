package com.test;

import java.util.Arrays;
import java.util.stream.IntStream;

class DotProduct {

  public static void main(String[] args ){
    dotProduct(10);
  }

  public static void dotProduct(int size) {

    int[] vectorA = new int[size];
    int[] vectorB = new int[size];

    Arrays.fill(vectorA, 10);
    Arrays.fill(vectorB, 20);

    int result = IntStream.range(0, vectorA.length)
      .parallel()
      .map( id -> vectorA[id] * vectorB[id])
      .reduce(0, Integer::sum);

    System.out.println(result);
  }
}

