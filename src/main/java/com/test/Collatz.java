package com.test;
public class Collatz {

    public static void main(String[] args) {
        tailCollatz(10);
        collatz(10);
        oneLineCollatz(10);
    }

    public static int tailCollatz(int n) {
        System.out.println(n);
        if(n < 1) return 0;
        else if(n == 1) return 1;
        else if (n % 2 == 0) return collatz(n / 2);
        else return collatz(3 * n + 1);
    }

    public static int collatz(int n) {
        System.out.println(n);
        if(n > 1 && n % 2 == 0) {
            return collatz(n/2);
        }
        else if (n > 1 && n % 2 != 0) {
            return collatz(3 * n + 1);
        }
        else {
            return 1;
        }
    }

    public static int oneLineCollatz(int n) {
        System.out.println(n);
        return n % 2 == 0 ? collatz(n/2) : collatz(3 * n + 1);
    }

}
