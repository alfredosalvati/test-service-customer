package com.test.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.test.ServiceTestBoot;
import com.test.dto.Customer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = ServiceTestBoot.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerControllerTest {

    private MockMvc mockMvc;

    @Autowired
    CustomerController customerController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(customerController)
                .build();
    }


    @Test
    public void createCustomer() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.registerModule(new ParameterNamesModule());
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        Customer customer = Customer.builder().customerId(123).createdAt(LocalDate.now().minusDays(12)).build();

        MvcResult mvcResult = mockMvc.perform(post("/customers/").contentType(APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(customer)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customerId", is(notNullValue())))
                .andReturn();

        Customer response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Customer.class);

        assertThat(response, is(notNullValue()));
        assertThat(response.getCustomerId(), is(123));
        assertThat(response.getExternalId(), is(notNullValue()));
        assertThat(response.getCreatedAt(), is(LocalDate.now().minusDays(12)));
    }

    @Test
    public void createCustomerWrongDate() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.registerModule(new ParameterNamesModule());
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        Customer customer = Customer.builder().customerId(123).createdAt(LocalDate.now().plusDays(12)).build();

        mockMvc.perform(post("/customers/").contentType(APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(customer)))
          .andExpect(status().is4xxClientError());
    }

    @Test
    public void createCustomerAndRetrieve() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.registerModule(new ParameterNamesModule());
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        Customer customer = Customer.builder().customerId(123).createdAt(LocalDate.now().minusDays(12)).build();

        MvcResult mvcResult = mockMvc.perform(post("/customers/").contentType(APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(customer)))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.customerId", is(notNullValue())))
          .andReturn();

        Customer response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Customer.class);


        mvcResult = mockMvc.perform(get("/customers/"+response.getCustomerId()))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.customerId", is(notNullValue())))
          .andReturn();

        response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Customer.class);

        assertThat(response, is(notNullValue()));
        assertThat(response.getCustomerId(), is(123));
        assertThat(response.getExternalId(), is(notNullValue()));
        assertThat(response.getCreatedAt(), is(LocalDate.now().minusDays(12)));

    }

    @Test
    public void NotFoundCustomer() throws Exception {
        //Not Found
        mockMvc.perform(get("/customers/123"))
          .andExpect(status().isNotFound())
          .andReturn();
    }
}
