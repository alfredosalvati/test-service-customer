# Test Service

The following test is to apply for a Soft. Eng. position at Alpian. 
Due some time limitations (and work) from my side, there are some outstanding tasks that could have been expanded:
- better error handling (e.g controller advice)
- better validation on user input
- more test coverage (unit test) and bdd testing 
- externalisation of the properties on YML
- introduction of cache

I'm happy to discuss this personally in a face to face interview. 


### Prerequisites

```
- Java 11 +
- Maven 3+
- Docker (Optional)

```


## Swagger EndPoint

http://localhost:8080/swagger-ui.html#/customer-controller

## EndPoints

- POST create customer:
http://localhost:8080/customers/

- GET customer:
http://localhost:8080/customers/[customerId]


### Installing

It can be run as a Docker container or as a Java Spring Boot application:

Using Docker: 

```
- unzip the folder
- cd [PATH]
- docker build -t test-service:1.0 .
- docker run -p 127.0.0.1:8080:8080 --name test-service -d test-service:1.0 
```

Using Java and Maven:
By Command line:

```
- unzip the folder
- cd [PATH]
- mvn clean install
- Run SpringTestBoot file 
```

Importing the project IDE

```
- unzip the folder
- Open the project (I used Intellij for this test)
- Run SpringTestBoot.
```

